"use strict";

const num1 = +prompt("Enter first value");
const num2 = +prompt("Enter second value");
const math = prompt("Enter math operation");

while (!num1) {
  num1 = +prompt("Enter first value, correctly");
}
while (!num2) {
  num2 = +prompt("Enter second value, correctly");
}
while (math !== "+" && math !== "-" && math !== "*" && math !== "/") {
  math = prompt("Choose among these operations: " + " " - (" " * " ") / " ");
}

function isMath(a, b, c) {
  switch (c) {
    case "+":
      return a + b;
    case "-":
      return a - b;
    case "*":
      return a * b;
    case "/":
      return a / b;
  }
}
console.log(isMath(num1, num2, math));
